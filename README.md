# Projeto 2: Criando um deploy de uma aplicação

Uma extensão do projeto 1, esse repositório adiciona alguns recursos como load balancer e secrets ao realizar o deploy. Neste projeto será realizado um deploy de uma aplicação completa com frontend, backend e database mysql. Adicionei um ci/cd do gitlab para exemplificar como irá funcionar o deploy da aplicação em um cluster kubernetes na GCP.