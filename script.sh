#!/bin/bash

echo "Buildando imagem php e subindo para o dockerhub..."

docker build . -t diegosantos144/php:1.1 backend/.
docker push diegosantos144/php:1.1

echo "Buildando imagem mysql e subindo para o dockerhub..."

docker build . -t diegosantos144/meubanco:1.1 database/.
docker push diegosantos144/meubanco:1.1

echo "Criando os serviços no cluster kubernetes..."

kubectl apply -f ./services.yml

echo "Criando os deploymentes..."

kubectl apply -f ./deployment.yml